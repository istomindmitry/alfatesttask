package alfatesttask;

import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDate;

import static io.restassured.RestAssured.*;

public class WeatherAPITest {

    private Response forecast;
    private City city;

    @BeforeClass
    public void setUp() {
        city = City.getMoscowCity();
        String url = city.getUrlForCity();
        this.forecast = given().header("X-Yandex-API-Key", "212d3143-ed31-4136-8119-fb2738f5c4de")
                .when().get(url);
    }

    @Test
    public void checkLatitude() {
        float expectedResult = Float.parseFloat(city.getLatitude());
        forecast.then().assertThat().body("info.lat", Matchers.equalTo(expectedResult));
    }

    @Test
    public void checkLongitude() {
        float expectedResult = Float.parseFloat(city.getLongitude());
        forecast.then().assertThat().body("info.lon", Matchers.equalTo(expectedResult));
    }

    @Test
    public void checkOffset() {
        int expectedResult = city.getOffset();
        forecast.then().assertThat().body("info.tzinfo.offset", Matchers.equalTo(expectedResult));
    }

    @Test
    public void checkTimeZoneName() {
        String expectedResult = city.getTimeZoneName();
        forecast.then().assertThat().body("info.tzinfo.name", Matchers.equalTo(expectedResult));
    }

    @Test
    public void checkTimeZoneAbbr() {
        String expectedResult = city.getAbbr();
        forecast.then().assertThat().body("info.tzinfo.abbr", Matchers.equalTo(expectedResult));
    }

    @Test
    public void checkTimeZoneDst() {
        boolean expectedResult = city.isDst();
        forecast.then().assertThat().body("info.tzinfo.dst", Matchers.equalTo(expectedResult));
    }

    @Test
    public void checkUrl() {
        String expectedResult = String.format("https://yandex.com/pogoda/?lat=%s&lon=%s", city.getLatitude(), city.getLongitude());
        forecast.then().assertThat().body("info.url", Matchers.equalTo(expectedResult));
    }

    @Test
    public void checkLimit() {
        int expectedResult = 2;
        forecast.then().assertThat().body("forecasts.size()", Matchers.equalTo(expectedResult));
    }

    @Test
    public void checkSeason() {
        LocalDate date = LocalDate.now();
        String expectedResult = Tools.getSeason(date);
        forecast.then().assertThat().body("fact.season", Matchers.equalTo(expectedResult));
    }

    @Test
    public void checkMoonCodeForSecondDay()
    {
        int moonCode = forecast.path("forecasts[1].moon_code");
        String expectedResult = Tools.getMoonText(moonCode);
        forecast.then().assertThat().body("forecasts[1].moon_text", Matchers.equalTo(expectedResult));
    }
}
