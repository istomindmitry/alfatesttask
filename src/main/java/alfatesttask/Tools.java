package alfatesttask;

import java.time.LocalDate;

public class Tools {
    private static final String seasons[] = {
            "winter", "winter",
            "spring", "spring", "spring",
            "summer", "summer", "summer",
            "autumn", "autumn", "autumn",
            "winter"
    };

    public static String getSeason(LocalDate date) {
        return seasons[date.getMonthValue()];
    }

    public static String getMoonText(int moonCode) {
        String moons[] = {
                "full-moon",
                "decreasing-moon", "decreasing-moon", "decreasing-moon",
                "last-quarter",
                "decreasing-moon", "decreasing-moon", "decreasing-moon",
                "new-moon",
                "growing-moon", "growing-moon", "growing-moon",
                "first-quarter",
                "growing-moon", "growing-moon", "growing-moon"};
        return moons[moonCode];
    }
}
