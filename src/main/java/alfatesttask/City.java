package alfatesttask;

public class City {
    private String latitude;
    private String longitude;
    private int offset;
    private String timeZoneName;
    private String abbr;
    private boolean dst;
    private String name;

    public City(String latitude, String longitude, int offset, String timeZoneName, String abbr, boolean dst, String name) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.offset = offset;
        this.timeZoneName = timeZoneName;
        this.abbr = abbr;
        this.dst = dst;
        this.name = name;
    }

    public static City getMoscowCity() {
        return new City("55.75396", "37.620393", 10800, "Europe/Moscow", "MSK", false, "Moscow");
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public int getOffset() {
        return offset;
    }

    public String getTimeZoneName() {
        return timeZoneName;
    }

    public String getAbbr() {
        return abbr;
    }

    public boolean isDst() {
        return dst;
    }

    public String getUrlForCity() {
        return String.format("https://api.weather.yandex.ru/v1/forecast?lat=%s&lon=%s&lang=en_US&limit=2", latitude, longitude);
    }
}